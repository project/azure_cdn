<?php

namespace Drupal\azure_cdn\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AzureCdnConfigForm.
 */
class AzureCdnConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'azure_cdn.azurecdnconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'azure_cdn_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Configurations stored via CMS
    $config = $this->config('azure_cdn.azurecdnconfig');
 
    $form['activate_azure_cdn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate Azure CDN'),
      '#description' => $this->t('If enabled, the media assets will be served via Azure CDN with support of Azure Blob Storage.'),
      '#default_value' => $config->get('activate_azure_cdn'),
    ];
    
    $form['default_scheme'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Scheme'),
      '#description' => $this->t('Please select the HTTP scheme to serve CDN Endpoint.'),
      '#options' => ['http' => $this->t('HTTP'), 'https' => $this->t('HTTPS')],
      '#size' => 1,
      '#default_value' => $config->get('default_scheme'),
    ];
    $form['azure_account_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Azure Account Name'),
      '#description' => $this->t('Please provide Azure Account Name.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('azure_account_name'),
    ];
    $form['azure_account_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Azure Account Key'),
      '#description' => $this->t('Provide Azure Account Key in order to authenticate with Azure Cloud.'),
      '#default_value' => $config->get('azure_account_key'),
    ];
    $form['azure_blob_container'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Azure Blob Container'),
      '#description' => $this->t('Please enter the Azure blob container, where all files will be uploaded.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('azure_blob_container'),
    ];
   
    $form['azure_cdn_enpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Azure CDN Enpoint'),
      '#description' => $this->t('Azure CDN URL to access media assets without http/https.'),
      '#default_value' => $config->get('azure_cdn_enpoint'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('azure_cdn.azurecdnconfig')
      ->set('activate_azure_cdn', $form_state->getValue('activate_azure_cdn'))
      ->set('default_scheme', $form_state->getValue('default_scheme'))
      ->set('azure_account_name', $form_state->getValue('azure_account_name'))
      ->set('azure_account_key', $form_state->getValue('azure_account_key'))
      ->set('azure_blob_container', $form_state->getValue('azure_blob_container'))
      ->set('azure_cdn_enpoint', $form_state->getValue('azure_cdn_enpoint'))
      ->save();
  }

}
