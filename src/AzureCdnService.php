<?php

namespace Drupal\azure_cdn;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\LocalStream;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\Url;
use Exception;
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Blob\BlobSharedAccessSignatureHelper;
use MicrosoftAzure\Storage\Blob\Models\CreateBlockBlobOptions;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
use MicrosoftAzure\Storage\Blob\Models\DeleteBlobOptions;
use MicrosoftAzure\Storage\Blob\Models\CreateBlobOptions;
use MicrosoftAzure\Storage\Blob\Models\GetBlobOptions;
use MicrosoftAzure\Storage\Blob\Models\ContainerACL;
use MicrosoftAzure\Storage\Blob\Models\SetBlobPropertiesOptions;
use MicrosoftAzure\Storage\Blob\Models\ListPageBlobRangesOptions;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Common\Exceptions\InvalidArgumentTypeException;
use MicrosoftAzure\Storage\Common\Internal\Resources;
use MicrosoftAzure\Storage\Common\Internal\StorageServiceSettings;
use MicrosoftAzure\Storage\Common\Models\Range;
use MicrosoftAzure\Storage\Common\Models\Logging;
use MicrosoftAzure\Storage\Common\Models\Metrics;
use MicrosoftAzure\Storage\Common\Models\RetentionPolicy;
use MicrosoftAzure\Storage\Common\Models\ServiceProperties;

/**
 * Class AzureCdnService.
 */
class AzureCdnService
{

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new AzureCdnService object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory)
  {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @method
   * This method returns if Azure CDN is activated in the configurations.
   * 
   * @return boolean
   *  Boolean value if Azure is activated.
   */
  public function isAzureCdnEnabled()
  {
    return $this->getAzureConfigurations()['activate_azure_cdn'] ?? false;
  }

  /**
   * @method
   *  This method returns a BlobRest Proxy Object.
   * 
   * @return MicrosoftAzure\Storage\Blob\BlobRestProxy
   *  An object of MicrosoftAzure\Storage\Blob\BlobRestProxy;
   */
  public function getAzureBlobClient()
  {
    return BlobRestProxy::createBlobService($this->getAzureConnectionString());
  }

  /**
   * @method
   *  This method returns an Azure Connectino String.
   */
  public function getAzureConnectionString()
  {
    // Connection String for Azure Connection.
    $connection_string = '';

    try {
      $config = $this->configFactory->get('azure_cdn.azurecdnconfig')->get();
      $connection_string = 'DefaultEndpointsProtocol=https;AccountName=' . $config['azure_account_name'] . ';AccountKey=' . $config['azure_account_key'];
    } catch (Exception $e) {
      \Drupal::logger('azure_cdn')->error($e->getMessage());
    }

    return $connection_string;
  }

  /**
   * @method
   *  This method returns the Azure End Point from the Drupal configurations.
   * 
   * @return string
   *  Qualified URL / CDN Endpoint
   */
  public function getAzureCdnEndpoint()
  {
    // Get the azure configurations from Drupal Config API.
    $config = $this->configFactory->get('azure_cdn.azurecdnconfig')->get();
    // Getting the Azure endpoint formed.
    return ($config['default_scheme'] ?? 'https') . '://' . ($config['azure_cdn_enpoint'] ?? '') . (DIRECTORY_SEPARATOR . $this->getAzureContainer());
  }

  /**
   * @method
   *  This method returns the array of Azure configurations stored via Config API.
   */
  public function getAzureConfigurations()
  {
    return $this->configFactory->get('azure_cdn.azurecdnconfig')->get();
  }

  /**
   * @method
   *  This method returns the azure container name.
   * 
   * @return string
   *  Azure container name of a blob.
   */

  public function getAzureContainer()
  {
    return $this->getAzureConfigurations()['azure_blob_container'] ?? 'dev';
  }

  /**
   * @method
   * This method uploads a blob to Azure Container.
   * 
   * @param string $file_uri
   *  This is the File URI from Drupal.
   * 
   */
  function uploadBlobFromDrupal($uri)
  {
    try {
      $blobClient = $this->getAzureBlobClient();
      // FileStream Wrapper.
      $filestream =  \Drupal::service('stream_wrapper_manager');
      // Base Directory default.
      $base_path = PublicStream::basePath(\Drupal::service('site.path'));
      // Target Path.
      $target_path = $filestream->getTarget($uri);
      // Complete Paths
      $complete_path = $base_path . DIRECTORY_SEPARATOR . $target_path;
      // Opening the file and fetching its content.
      $file = fopen($complete_path, "r");
      // Uploading the blob to Azure Container.
      $blobClient->createBlockBlob($this->getAzureContainer(), $target_path, $file);
    } catch (ServiceException $e) {
      \Drupal::logger('azure_cdn')->error($e->getMessage());
    }
  }
  
  /**
   * @method
   *  This method validates the CDN only for non-admin pages.
   */
  public function isCdnApplicable()
  {
    // Fetching the route to match against Admin URLs.
    $route = \Drupal::routeMatch()->getRouteObject();
    // Default case is False.
    $is_admin = FALSE;
    
    // If route is empty.
    if (!empty($route)) {
      $is_admin_route = \Drupal::service('router.admin_context')->isAdminRoute($route);
      $has_node_operation_option = $route->getOption('_node_operation_route');
      $is_admin = ($is_admin_route || $has_node_operation_option);
    } 
    // If route is not empty.
    else {
      $current_path = \Drupal::service('path.current')->getPath();
      if (preg_match('/node\/(\d+)\/edit/', $current_path, $matches)) {
        $is_admin = TRUE;
      } elseif (preg_match('/taxonomy\/term\/(\d+)\/edit/', $current_path, $matches)) {
        $is_admin = TRUE;
      }
    }
    return !$is_admin;
  }
}
