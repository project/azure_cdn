# AZURE CDN
This module is created to push your media & assets to Azure Blob Container. 

## Features (WIP)
This section will gives information regarding the features of initial phase.
### Completed Features
- Configurations for Azure Blob Container.
- Activation and deactivation via admin configurations.
- Serves the images with Azure CDN endpoint.
- Upload available images on a page to Azure Blob Storage automaticallly.

### Features in-progress
- Removal of Azure Blob on deletion of local media assets.
- Batch process to upload bulk media assets to Azure Blob Storage.

## Pre-requisites
- You would require an Azure Blob Storage account and a CDN endpoint already created.
- Following Libraries Need to be downloaded via composer.
-- "microsoft/azure-storage-blob"
-- "microsoft/azure-storage-file"

## Steps to Use
1. Go to the URL /admin/modules and search for azure_cdn.
2. Install the module.
3. Configure the module by going to /admin/config/azure-cdn/configurations.
4. Once that's done, clear your cache.

## Module's Working
1. To change the URLs, hook_url_alter is used.
2. It checks if the configurations are enabled and support is activated.
3. If activated it checks if the local file exist in the system, if yes, it will upload it to the Azure CDN and would serve the file afterwards from there.
4. For missing files it does not work.

to be continued...
